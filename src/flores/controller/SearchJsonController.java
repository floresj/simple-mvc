package flores.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import flores.model.Account;
import flores.service.AccountService;
/**
 * Servlet implementation class SearchController
 */
@WebServlet("/Search.json")
public class SearchJsonController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	AccountService accountService = new AccountService();
	ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idParam = request.getParameter("id");
		if (idParam == null || idParam.isEmpty()){
			response.getWriter().write("Id is required");
			return;
		}		
		Account account = accountService.findById(Long.parseLong(idParam));		
		response.setContentType("application/json");
		response.getWriter().write(mapper.writeValueAsString(account));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
